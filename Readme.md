../////////////////..
# Jenkins CI Step by Step
---

Jenkins is an open source Continuous Integration server capable of orchestrating a chain of actions that help to achieve the Continuous Integration process and facilitating technical aspects of continuous delivery/deployment in an automated fashion.

Jenkins is free and is entirely written in Java. Jenkins is a widely used application around the world. It provides hundreds of plugins to support building, deploying and automating any project.


#### Learning Path
To learn Jenkins CI, we propose the following path. It contains step by step practical labs starting from the initial setup and finishing by building complete pipelines for Continious Integration, Continious Delivery and Continious Deployement. 

##### Jenkins SetUp using Vagrant and Ansible

|   Steps | Objectives |
|---------|------------| 
| *01-jenkins-install-ubuntu-Shell* | How to install Jenkins on Ubuntu 18 using Vagrant and Shell Script |
| *02-jenkins-install-ubuntu-Ansible-Playbook* | How to install Jenkins on Ubuntu 18 using Vagrant and an Ansible Playbook  |
| *03-jenkins-install-ubuntu-Ansible-Roles* | How to install Jenkins on Ubuntu 18 using Vagrant and an Ansible Galaxy Role  |

##### Jenkins Declarative Pipelines Syntax
|   Steps | Objectives |
|---------|------------| 
|*04-jenkins-pipelines-declarative-01-hello* | Hello World Jenkins Declarative Pipelines|
| *04-jenkins-pipelines-declarative-02-parameters* | Building parametrized pipelines|
| *04-jenkins-pipelines-declarative-03-tools* | Tools section in Jenkins pipelines |
| *04-jenkins-pipelines-declarative-04-input*  | Input data within interactive pipelines |
| *04-jenkins-pipelines-declarative-05-when* | Conditional pipelines, stages and steps with when |
| *04-jenkins-pipelines-declarative-06-parallel-stages* | Implementing parallel stages |
| *04-jenkins-pipelines-declarative-07-post* | Defining Post build actions |

   
##### Jenkins Distributed Builds
 |   Steps | Objectives |
 |---------|------------|
 | *05-jenkins-distributed-builds* | Implementing Distributed Master/Slave builds |
 
##### Enhance Pipelines User Experience with Blue Ocean 
|   Steps | Objectives |
|---------|------------|
| *06-jenkins-Blue-Ocean-Plugin* | Installing and Using the Blue Ocean user interface |

##### Jenkins and Maven Plugin for Quality Assurance
|   Steps | Objectives |
|---------|------------|
| *07-jenkins-maven-plugin-01-unit-and-integration-tests* | Implementing unit tests and integration tests in separate Jenkins stages |
| *07-jenkins-maven-plugin-02-static-code-analysis-sonarqube* | Performing static code analysis using maven and SonarQube |
| *07-jenkins-maven-plugin-03-performance-tests-jmeter* | Performing performance tests using maven and Apache JMeter |
|  *07-jenkins-maven-plugin-04-artefact-deployment-artifactory* | Deploying maven artefcats into the artifactory repostory   |
##### Jenkins Distributed Builds 
|   Steps | Objectives |
|---------|------------|
| *08-jenkins-cicd-pipeline-maven-01-continious-integration* | Demo of a complete **continious integration** pipeline using Jenkins  |
| *08-jenkins-cicd-pipeline-maven-02-continious-delivery* | Demo of a complete **continious delivery** pipeline using Jenkins |
| *08-jenkins-cicd-pipeline-maven-03-continious-deployment* | Demo of a complete **continious deployment** pipeline using Jenkins |

 
 
 

