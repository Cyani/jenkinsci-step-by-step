#!/bin/bash
 
# Update and Upgrade all the packages
sudo apt update -y
sudo apt upgrade -y

# Install Java.
sudo apt install openjdk-8-jdk -y

# Add the Jenkins Debian repository.
# - First Import the GPG keys of the Jenkins repository 
wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
# Next, add the Jenkins repository to the system with:
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'

# Update the apt package list and install the latest version of Jenkins by typing:
sudo apt update -y
sudo apt install jenkins -y

# Display the initial admin password
sudo cat /var/lib/jenkins/secrets/initialAdminPassword

# verify it by printing the service status:
# systemctl status jenkins