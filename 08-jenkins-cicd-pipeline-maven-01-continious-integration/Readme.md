# JenkinsCI : Continious Integration
---
##The CI pipeline
We are now at the heart of the CI design. We will be creating a  Pipeline in Jenkins that will have the following stages:
1. Fetch the code from the version control system (VCS) on a push event
(initialization of the CI pipeline).
2. Build and unit test the code, and publish a unit test report on Jenkins.
3. Perform static code analysis on the code and upload the result to SonarQube. Fail
the pipeline if the number of bugs crosses the threshold defined in the quality
gate.
4. Perform integration testing and publish a unit test report on Jenkins.
5. Upload the built artifacts to Artifactory along with some meaningful properties.
The figure below shows the stages of the pipeline.
![Pipeline](images/pipelineStages.png)

##Toolset for CI
The example project for which we are implementing CI is a simple Maven project. In
this chapter, we will see Jenkins working closely with many other tools. The following table
contains the list of tools and technologies involved in everything that we will be seeing:
   The example project for which we are implementing CI is a simple Maven project. 
   
   |   Tool | Role |
|---------|------------| 
|**Java**  |Primary programming language used for coding |
|**Maven** |Build tool|
|**JUnit** |Unit testing and integration testing tools |
|**Jenkins**| Continuous Integration tool|
|**GitLab** |Version control system |
|**SonarQube** |Static code analysis tool |
| **JFrog Artifactory** |Binary repository manager|
   
   
## Implementation details

See **Chapter 7: Continuous Integration Using Jenkins** of *Nikhil Pathania* Book **Continuous Integration Using Jenkins, 2nd Edition**

![book](images/book.jpg)

