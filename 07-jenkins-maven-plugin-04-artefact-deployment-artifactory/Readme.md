# Jenkins Maven Tool - Artifact deployment with JFrog Artifactory
---

Artifactory is a product by JFrog that serves as a [binary repository manager] (https://en.wikipedia.org/wiki/Binary_repository_manager). The binary repository is a natural extension to the source code repository, in that it will store the outcome of your build process, often denoted as artifacts. Most of the times one would not use the binary repository directly but through a package manager that comes with the chosen technology (Java, Node, Python, ...). The importance of Artifactory can be understood in relation to the philosophy of DevOps to "*Build once, Deploy always*". It goes a long way in Continuous Integration to build your binary once, put it into Artifactory and then call it from there to deploy into all of the different environments. That way, we are sure that the code that works in Dev is the one pushed to Prod and will work there.
## Installing JFrog Artifactory
-  [Download](https://api.bintray.com/content/jfrog/artifactory/jfrog-artifactory-oss-$latest.zip;bt_package=jfrog-artifactory-oss-zip) the Open Source version of JFrog Artifactory and Unzip it.
  
      > **Requirements**:  
        JFrog Artifactory requires Java. Make sure that your **JAVA_HOME** environment variable is correctly set to your JDK installation.
   
- To Run Artifactory, go to `bin/` folder under the Artifactory installation directory and run the `artifactory.bat` (Windows) or `artifactory.sh` (Unix/Linux). 
Read more [here](https://www.jfrog.com/confluence/display/RTF/Getting+Started) on how to get started with JFrog Artifactory.

## Installing Jenkins Artifactory Plugin

- To install the Jenkins Artifactory Plugin, go to *Manage Jenkins > Manage Plugins, click on the Available tab and search for Artifactory*. Select the Artifactory plugin and click *Download Now* and I*nstall After Restart*.
- Artifactory Plugin Configuration
To configure your Artifactory server settings, go to the **Jenkins System Configuration** Page (*Manage Jenkins > Configure System*).
Click the Add Artifactory Server button to create a new Artifactory server configuration ![this tutorial](images/ArtifactoryConfiguration.png).

- Assign a Server ID, specify the URL and default credentials to the artifactory server.

## Configuring Jenkins Arifact Deployment stage

- **Maven Configuration**
No special Maven configuration for JMeter is required. However, if you want to use Artifactory as a Proxy for maven, you should add **settings.xml** under your **.m2 folder** (located under the user home directory)
This is un example of settings.xml configuration:
  ```xml
  <settings>
    <repositories>
    <repository>
        <id>central</id>
        <url>http://localhost:8081/artifactory/libs-release</url>
        <snapshots>
            <enabled>false</enabled>
        </snapshots>
    </repository>
    <repository>
        <id>snapshots</id>
        <url>http://localhost:8081/artifactory/libs-snapshot</url>
        <releases>
            <enabled>false</enabled>
        </releases>
    </repository>
  </repositories>
  <pluginRepositories>
    <pluginRepository>
        <id>central</id>
        <url>http://localhost:8081/artifactory/plugins-releases</url>
        <snapshots>
            <enabled>false</enabled>
        </snapshots>
    </pluginRepository>
    <pluginRepository>
        <id>snapshots</id>
        <url>http://localhost:8081/artifactory/plugins-snapshots</url>
        <releases>
            <enabled>false</enabled>
        </releases>
    </pluginRepository>
   </pluginRepositories>
   <mirror>
      <id>artifactory</id>
      <mirrorOf>*</mirrorOf>
      <url>http://localhost:8081/artifactory/repo</url>
      <name>Artifactory</name>
    </mirror>
   </settings>
  ``` 

- **Jenkins Performance Stage**
 Jenkins uses Artifactory plugin to deploy the artifact in Artifactory. More details of the pipelinen configuration are given in the [official Artifactory documentation](https://www.jfrog.com/confluence/display/RTF5X/Working+With+Pipeline+Jobs+in+Jenkins). 
 This is the pipeline that demonstrates how to publish maven artifacts in JFrog Artifactory.
  ``` groovy
   pipeline {
    agent any    
    tools {
      maven 'apache-maven-3.6.3' 
    }    
    stages {    	 
	    stage ('Clone') {
          steps {
               git branch: 'master', url: "https://gitlab.com/mromdhani/07-jenkins-maven-plugin-04-artefact-deployment-artifactory.git"
           }
	    }	 
	   stage('Build & Unit test'){
		  steps {
             bat 'mvn clean verify -DskipITs=true';
             junit '**/target/surefire-reports/TEST-*.xml'
             archiveArtifacts  'target/*.jar'
	      }
   	   }
       stage ('Publish'){
	     steps {
		    script {
                 def server = Artifactory.server('My Default Artifactory Server')
                 def rtMaven = Artifactory.newMavenBuild()
                 rtMaven.resolver server: server, releaseRepo: 'libs-release', snapshotRepo: 'libs-snapshot'
                 rtMaven.deployer server: server, releaseRepo: 'libs-release-local', snapshotRepo: 'libs-snapshot-local'
                 rtMaven.tool = 'apache-maven-3.6.3'
                 def buildInfo = rtMaven.run pom: 'pom.xml', goals: 'install'
                 server.publishBuildInfo buildInfo
              }
	      }
	  }
    }
  }
  ```

- **Analyzing the results**
  The figure below shows the Jenkins log informing that the Deployment of the artifact has succeeded.
  ![alt test](images/JenkinsLog.png) )
  The following figure the deployed artifact on the Artifactory dashboard.
  ![alt test](images/DeployedArtifact.png))
