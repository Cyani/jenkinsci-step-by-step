# Jenkins Maven Tool - Code Analsis with SonarQube
---

## Install and Start  SonarQube Community Edition:

- **Installing from a zip file**
   * [Download](https://www.sonarqube.org/downloads/) the SonarQube Community Edition.

   * As a non-root user, unzip it, let's say in **C:\sonarqube** or **/opt/sonarqube**.

    * As a non-root user, start the SonarQube Server:

      > On Windows, execute:
      `C:\sonarqube\bin\windows-x86-xx\StartSonar.bat`

      > On other operating systems, as a non-root user execute:
      `opt/sonarqube/bin/[OS]/sonar.sh console`
 
      If your instance fails to start, check your logs to find the cause.

   *  Log in to `http://localhost:9000` with System Administrator credentials (login=`admin`, password=`admin`).


- **Using Docker**
A Docker image of the Community Edition is available on [Docker Hub](https://hub.docker.com/_/sonarqube/). You can find usage and configuration examples there.

## Configure Static Software Analysis Stage

- **Maven Configuration**
No special Maven configuration for SonarQube is required. Maven will from its repositor retrieve the last stable version of the Sonar Scanner Plugin. 

- **Jenkins SonarQube Stage**
  The SonarQube static analyser is lauched using the `mvn sonar:sonar` command. After executing the command, the results will be available on the Projects dashboard  at `http://localhost:9000`.

   ``` groovy
   stage('Static Code Analysis'){
	  steps {
    	bat "mvn clean verify sonar:sonar -Dsonar.projectName=07-static-code-analysis-sonarqube -Dsonar.projectKey=07-static-code-analysis-sonarqube -Dsonar.projectVersion=$BUILD_NUMBER";
	 }
	}
  ```

- **Analyzing the results**
  Now that we've analyzed our first project, we can go to the web interface at `http://localhost:9000` and refresh the page.

  There we'll see the report summary:
![alt test](images/sonarqubeResults.jpg "SonarQube results")
Discovered issues can either be a Bug, Vulnerability, Code Smell, Coverage or Duplication. Each category has a corresponding number of issues or a percentage value.

Moreover, issues can have one of five different severity levels: blocker, critical, major, minor and info. Just in front of the project name is an icon that displays the Quality Gate status – passed (green) or failed (red).

Clicking on the project name will take us to a dedicated dashboard where we can explore issues particular to the project in greater detail.

We can see the project code, activity and perform administration tasks from the project dashboard – each available on a separate tab.

- **SonarQube Quality Gate**

  From the web interface, the Quality Gates tab is where we can access all the defined quality gates. By default, SonarQube way came preinstalled with the server.

  The default configuration for SonarQube way flags the code as failed if:

   * the coverage on new code is less than 80%
   * percentage of duplicated lines on new code is greater than 3
   * maintainability, reliability or security rating is worse than A

  With this understanding, we can create a custom Quality Gate.